# DATA STRUCTURES AND ALGORITHMS USING PYTHON #

This repository contains Python code for various data structures and algorithms 
than are commonly used to solve different problems.

### Pre requisites ###

* Basic knowledge in programming 
* Python 3. Run command `python --version` to check python version.
* Your favorite IDE 
* Command line terminal

# CONTENT #